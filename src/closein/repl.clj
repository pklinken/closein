(ns closein.repl
  (:require [clojure.string]))

(defn print-help
  []
  (println "Available commands: ")
  (println "m direction - Move a player in a given direction: \"m north\", \"m east\", \"m e\"")
  (println "l - List all items available for pickup on this location")
  (println "i - List inventory")
  (println "d number - Drop item from inventory")
  (println "p number - Pick up item, add a number if there are multiple items: \"p\", \"p 2\"")
  (println "r - repeat last command")
  (println "h - print this text")
  (println "s - skip turn")
  (println "a direction - Attack in the given direction")
  (println "M - switch to arrow-key move mode, where arrow keys move/attack")
  (println "q - Quit the program"))

(defn sanitize-user-input
  [input]
  (if (not (nil? input))
    (->> input
         clojure.string/trim
         (re-seq #"[a-zA-Z0-9\s]")
         (clojure.string/join))))

(defn create-command
  "Returns a map of the form {:command :move :args [:north]}"
  [command & args]
  {:command command :args (vec args)})

(defn create-direction-command
  "Returns a command of the form {:move :north} if direction is valid, nil otherwise"
  [command direction]
  (if-let [dir (case direction
             ("n" "north") :north
             ("e" "east") :east
             ("s" "south") :south
             ("w" "west") :west
             false)]
    (create-command command dir)))

(defn create-opt-num-arg-command
  [command num]
  (if (nil? num)
    (create-command command 0)
    (if-let [num (try 
                      (Integer/parseInt 
                       (re-find #"\d*" num)) 
                      (catch NumberFormatException e false))]
      (create-command command num)
      (create-command command 0))))

(defn parse-user-input
  "Parses sanitized input and attempts to translate this input to a command"
  [input]
  (if (not (nil? input))
    (let [tokens (clojure.string/split input #" ")]
      (case (first tokens)
        ("m" "move") (create-direction-command :move (second tokens))
        ("p" "pickup") (create-opt-num-arg-command :pickup (second tokens))
        ("r") (create-command :repeat)
        ("i") (create-command :inventory)
        ("l" "list") (create-command :list-items)
        ("d" "drop") (create-opt-num-arg-command :drop (second tokens))
        ("q" "quit") (create-command :quit)
        ("h" "help") (create-command :help)
        ("s") (create-command :skip)
        ("a") (create-direction-command :attack (second tokens))
        ("M") (create-command :move-mode)
        nil))))

(defn get-user-input
  "Prompts for and returns user input"
  []
  (print "?> ")
  (flush)
  (let [user-input (read-line)]
    (if (seq? user-input)
      user-input)))
