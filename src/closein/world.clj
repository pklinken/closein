(ns closein.world
  (:require [closein.entity :as entity]))

(defrecord World [width height cells])
(defrecord WorldCell [entity items])

(defn create-worldcells
  [width height]
  ;; Using refs here because we'll be wanting to do transactions involving multiple cells
  ;; for example, one player hitting another player
  (vec (take (* height width) (repeatedly #(ref (WorldCell. nil #{}))))))

(defn create-world
  [width height]
  {
   :height height,
   :width width,
   :cells (create-worldcells width height)
   }
  )

(defn get-worldcell
  [world x y]
  (if (not (or (>= x (:width world))
               (< x 0)
               (>= y (:height world))
               (< y 0)))
      (nth (:cells world) (+ (* y (:width world)) x) nil)))

(defn get-worldcell-coords
  [world worldcell]
  (let [cell-index (.indexOf (:cells world) worldcell)]
    (if (>= cell-index 0)
      [(mod cell-index (:width world)) (int (/ cell-index (:width world)))])))

(defn worldcell-neighbour
  "Return the neighbouring cell in the given direction of worldcell in world, or nil if this does not exist"
  [world worldcell direction]
  (let [cell-count (* (:height world) (:width world))]
    (if-let [cell-location (.indexOf (:cells world) worldcell)]
      (let [[x y] (get-worldcell-coords world worldcell)]
        (case direction
          :north (get-worldcell world x (dec y))
          :east (get-worldcell world (inc x) y)
          :south (get-worldcell world x (inc y))
          :west (get-worldcell world (dec x) y))))))

(defn is-ref?
  "Returne true if the given argument is an instance of clojure.lang.Ref"
  [x]
  (instance? clojure.lang.Ref x))

(defn has-entity?
  "Return true if the given worldcell contains an entity"
  [worldcell]
  (not (nil? (:entity
              (if (is-ref? worldcell)
                @worldcell
                worldcell)))))

(defn find-entity
  [world entity]
  (first (filter #(= (:entity @%) entity) (:cells world))))

(defn find-player
  [world]
  (first (filter #(not (nil? (get-in @% [:entity :name]))) (:cells world))))

(defn all-entities
  [world]
  (filter #(not (nil? %)) (map (comp :entity deref) (:cells world )))
  )

(defn has-item?
  "Return true if the given worldcell contains an item"
  [worldcell]
  (seq? (:items
      (if (is-ref? worldcell)
        @worldcell
        worldcell))))


(defn print-worldcell
  [worldcell]
  (print (cond
           (has-entity? worldcell) (entity/print-entity (:entity @worldcell))
           (has-item? worldcell) "+"
           :else ".")))

(defn print-world
  [world]
  (doall (for [y (range (:height world)) x (range (:width world))]
           (do (print-worldcell (get-worldcell world x y))
               (if (= x (dec (:width world))) (println))))))

