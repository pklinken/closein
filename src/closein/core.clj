(ns closein.core
  (:gen-class)
  (:require [closein.ai :refer [run-ai]]
            [closein.entity :as entity]
            [closein.item :as item]
            [closein.repl :as repl]
            [closein.world :as world]))

(def using-move-mode (atom false))
(def DEFAULT_WORLD_WIDTH 40)
(def DEFAULT_WORLD_HEIGHT 20)

(defn create-entity
  ([species max-hp ap]
   (entity/->Entity species (ref max-hp) max-hp ap 5))
  ([species max-hp ap aggro-range]
   (entity/->Entity species (ref max-hp) max-hp ap aggro-range)))

(defn create-player
  ([name max-hp]
   (entity/->Player name (ref #{}) (ref max-hp) max-hp 0))
  ([name max-hp ap]
   (entity/->Player name (ref #{}) (ref max-hp) max-hp ap)))

(defn add-entity-to-world
  "Adds a player to a world, returns true on success"
  [entity world x y]
  (if-let [worldcell (world/get-worldcell world x y)]
    ;; Now that we have a worldcell we must check if it already contains
    ;; a player or another entity
    (if (not (world/has-entity? worldcell))
      ;; There is no entity in this cell, we can safely add a player here
      (dosync
       (alter worldcell assoc-in [:entity] entity)))
    ))

(defn remove-entity
  [worldcell]
  (world/map->WorldCell (dissoc worldcell :entity)))

(def directions [:north :east :south :west])

(defn move-entity
  "Attempts to move the player one step in the given direction"
  [world entity direction]
  (if-let [entity-location (world/find-entity world entity)]
    (if-let [entity-destination (world/worldcell-neighbour world entity-location direction)]
      (if (not (world/has-entity? entity-destination))
        (dosync
         ;; Add the player to the destination cell
         (alter entity-destination assoc-in [:entity] entity)
         ;; Remove the player from its current cell
         (alter entity-location remove-entity)))
      )))

(defn attack
  "Attempts to attack something by slashing a sword in the given direction"
  [world entity direction]
  (let [entity-location (world/find-entity world entity)]
    (if-let [neighbour (world/worldcell-neighbour world entity-location direction)]
      (if-let [enemy (:entity @neighbour)]
        ;; THERE exists a neighbouring cell with an entity, attack!
        ;; This involves working out AP values and doing a transaction/rolling dice/etc
        (do  (println "Attacking " (entity/entity-name enemy))
             (dosync
              (alter (:hp enemy) - (entity/calculate-ap entity))))
        ))))

(defn list-player-inventory
  [player]
  (let [sorted-items (item/sort-item-list @(:inventory player))]
        (map #(item/print-item (first %) (second %)) (zipmap (range (count sorted-items)) sorted-items)))
  )

(defn print-player-status
  [player]
  (println 
   (if (entity/is-alive? player)
     (format "%s - HP: %d(%d) AP: (%d)" 
             (entity/entity-name player) 
             @(:hp player) 
             (:max-hp player) 
             (entity/calculate-ap player))
     (format "%s - Dead." (entity/entity-name player)))))

(defn enable-move-mode
  []
  (reset! using-move-mode true)
  )

(defn disable-move-mode
  []
  (reset! using-move-mode false)
  )

(defn handle-move-mode-input
  []
  
  )

(defn quit-program
  []
  (println "Bye!")
  (System/exit 0))

(defn not-implemented
  []
  (println "This command has not yet been implemented"))

(defn apply-command
  [world player cmd]
  (if (entity/is-alive? player)
    (case (:command cmd)
      (:move) (move-entity world player (first (:args cmd)))
      (:list-items) (dorun (item/list-items world player))
      (:quit) (quit-program)
      (:repeat) (not-implemented)
      (:pickup) (item/pickup-item world player (first (:args cmd)))
      (:help) (repl/print-help)
      (:drop) (item/drop-item world player (first (:args cmd)))
      (:inventory) (dorun (list-player-inventory player))
      (:skip) (println (entity/entity-name player) "Skipping this turn")
      (:attack) (attack world player (first (:args cmd)))
      (:move-mode (enable-move-mode))
      (println "Error parsing command")
      )))

(defn -main
  []
  (let [w (world/create-world DEFAULT_WORLD_WIDTH DEFAULT_WORLD_HEIGHT)
        p (create-player "Hypofyse" 200 50)
        e (create-entity :imp 150 15)
        i (item/create-item "The Stoppable Force"
                       "+1 weapon" 3.5 10000 :weapon [] [])]
    (add-entity-to-world p w (rand-int DEFAULT_WORLD_WIDTH) (rand-int DEFAULT_WORLD_HEIGHT))
    (add-entity-to-world e w (rand-int DEFAULT_WORLD_WIDTH) (rand-int DEFAULT_WORLD_HEIGHT))
    (item/add-item-to-world i w (rand-int DEFAULT_WORLD_WIDTH) (rand-int DEFAULT_WORLD_HEIGHT))
    (while (= true true)
      (print-player-status p)
      (world/print-world w)
      (item/list-items w p)
      (if @using-move-mode
        (handle-move-mode-input)
        (apply-command w p (-> (repl/get-user-input)
                               (repl/sanitize-user-input)
                               (repl/parse-user-input))))
      (run-ai w (partial apply-command w)))
    
    )
  (println "This is the end of the program."))


;; (def w (create-world 3 4))
;; (def p (create-player "hyp" 200))
;; (def e (create-entity :imp 50 75))
;; (def i (create-item  "sword" "simple sword" 3.5 10000 :weapon [] []))
;; (add-entity-to-world p w 0 1)
;; (add-entity-to-world e w 2 3)
;; (add-item-to-world i w 0 0)
;; (print-world w)


