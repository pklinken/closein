(ns closein.item
  (:require [closein.world :as world]))

;;; Items should have an ID when they are created, we don't want this 
;;; burden to befall the caller, so we kind of need a key-value store.. ?
;;; Probably not, but we do need some unique ID... ?

(defrecord Item [id name desc weight usages type effects_on_self effects_on_target])

(defn create-item
  [name desc weight usages type effects_on_self effects_on_target]
  (Item. (java.util.UUID/randomUUID)
         name desc weight usages type
         effects_on_self effects_on_target)
  )

(defn add-item-to-world
  [item world x y]
  (if-let [worldcell (world/get-worldcell world x y)]
    (dosync
     (alter worldcell update-in [:items] conj item))
    ))

(defn print-item
  [index item]
  (println
   (format "[%d]: %s %s %f %d" index (:name item) (:desc item)
           (:weight item) (:usages item))))

(defn sort-item-list
  [items]
  (sort-by :id items))

(defn list-items
  [world player]
  (if-let [worldcell (world/find-entity world player)]
    (if (seq? (:items @worldcell))
      (let [sorted-items (sort-item-list (:items @worldcell))]
        (doall (map #(print-item (first %) (second %)) (zipmap (range (count sorted-items)) sorted-items)))))))

(defn pickup-item
  "Transfers the num item in the worldcell which contains player to player"
  [world player num]
  (if-let [worldcell (world/find-entity world player)]
    (if-let [item (nth (sort-item-list (:items @worldcell)) num nil)]
      (dosync
       (alter worldcell update-in [:items] disj item)
       (alter (:inventory player) conj item))))
  )

(defn drop-item
  "Transfers the num item in the player's inventory to the worldcell which contains player"
  [world player num]
  (if-let [worldcell (world/find-entity world player)]
    (if-let [item (nth (sort-item-list @(:inventory player)) num nil)]
      (dosync
       (alter (:inventory player) disj item)
       (alter worldcell update-in [:items] conj item)))))
