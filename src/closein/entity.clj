(ns closein.entity)
(defprotocol EntityAbilities
  "The abilities of an entity, player or monster"
  (entity-name [entity]))


(defprotocol PrintEntity
  "Print an entity in accordance with its type and status"
  (print-entity [entity]))

(defprotocol EntityAP
  "Defines functions regarding AP on entities"
  (calculate-ap [entity]))

(defprotocol EntityAlive
  "Is the entity alive ?"
  (is-alive? [entity]))

(def ansi-styles
  {:red   "[91m"
   :green "[92m"
   :yellow "[93m"
   :blue  "[94m"
   :reset "[0m"})

(defn ansi
  "Produce a string which will apply an ansi style"
  [style]
  (str \u001b (style ansi-styles)))

(defn colorize
  "Apply ansi color to text"
  [text color]
  (str (ansi color) text (ansi :reset)))

(defn calculate-ansi-color-by-perc
  "Returns a symbol like :red depending on the percentage of current has in maximum"
  [current maximum]
  (let [perc (* 100 (/ current maximum))]
    (cond
      (< perc 40) :red
      (< perc 80) :yellow
      :else :green))
  )

(defrecord Player [name inventory hp max-hp ap]
  PrintEntity
  (print-entity [entity]
    (if (is-alive? entity)
      (let [color (calculate-ansi-color-by-perc
                   @(:hp entity)
                   (:max-hp entity))]
        (colorize "P" color))
      "*"))
  EntityAbilities
  (entity-name [entity]
    (:name entity))
  EntityAP
  (calculate-ap
    [entity]
    (:ap entity)
    )
  EntityAlive
  (is-alive?
    [entity]
    (> @(:hp entity) 0)))

(defrecord Entity [species hp max-hp ap aggro-range]
  PrintEntity
  (print-entity [entity]
    (if (is-alive? entity) 
      (let [color (calculate-ansi-color-by-perc
                   @(:hp entity)
                   (:max-hp entity))]
        (colorize "B" color))
      "*"))
  EntityAbilities
  (entity-name [entity]
    (:species entity))
  EntityAP
  (calculate-ap
    [entity]
    (:ap entity))
  EntityAlive
  (is-alive?
    [entity]
    (> @(:hp entity) 0)))

