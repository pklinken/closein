(ns closein.ai
  (:require closein.repl
            closein.world
            closein.entity))

(defn get-player-direction
  "Returns the direction of the player to entity if it is in a neighbouring cell or nil if none is found"
  [world entity-location player-location]
  (first (filter #(= (closein.world/worldcell-neighbour world entity-location %) player-location) [:north :east :south :west])))

(defn get-player-general-direction
  "Returns the general direction of the player to entity if the player is within aggro-range, used when the player is not a neighbour to the entity"
  [world entity entity-location player-location]
  ;; Although this kind of depends on the implementation, we can hopefully safely assume that the properties of a 2d plane wont change
  (let [[ex ey] (closein.world/get-worldcell-coords world entity-location)
        [px py] (closein.world/get-worldcell-coords world player-location)
        [vx vy] [(- ex px) (- ey py)]]
    (if (or (< (Math/abs vx) (:aggro-range entity))
            (< (Math/abs vy) (:aggro-range entity)))
      (if (= (Math/abs vx) (Math/abs vy))
        ;; If distance between player and entity is equal on both axis, pick a random axis
        (if (= 0 (rand-int 2))
          (if (> vx 0)
            :west
            :east)
          (if (> vy 0)
            :north
            :south))
        ;; Else try to close the distance on the axis with the largest distance first
        (if (> (Math/abs vx) (Math/abs vy))
          ;; Largest distance on X axis
          (if (> vx 0)
            :west
            :east)
          (if (> vy 0)
            :north
            :south))))))

(defn next-command
  "Determine the next command for the entity in this world"
  [world entity]
  (let [entity-location (closein.world/find-entity world entity)]
    (if-let [player-location (closein.world/find-player world)]
      (if-let [player-direction (get-player-direction world entity-location player-location)]
        (closein.repl/create-command :attack player-direction)
        (if-let [player-direction (get-player-general-direction world entity entity-location player-location)]
          (closein.repl/create-command :move player-direction)
          (closein.repl/create-command :skip))))))

(defn run-ai
  "Run an AI cycle for each non-player entity in the world"
  [world f-apply-command]
  (let [monsters (filter
                  #(not (nil? (:species %)))
                  (closein.world/all-entities world))]
    (doall (map #(f-apply-command % (next-command world %)) monsters))
    ;; (map #({:entity % :command (next-command world %)}) monsters)
    ))
