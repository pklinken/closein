(ns closein.core-test
  (:require [clojure.test :refer :all]
            [closein.core :refer :all]
            [closein.world :refer :all]))

;; (deftest a-test
;;   (testing "FIXME, I fail."
;;     (is (= 0 1))))

(deftest is-ref?-test
  (testing "is-ref?"
    (is (= (is-ref? 3) false))
    (is (= (is-ref? (ref 3)) true))))
